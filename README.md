# 异步爬虫

#### 介绍
爬虫程序，使用多线程实现

#### 软件架构
用到了springboot，jsoup和async多线程


#### 使用说明

运行CrawelApplication即可，页面访问http://localhost:8080/crawler/zhainanfuliDownloadImages?start=1&end=1即可下图

#### 对应教程

我的知乎专栏 [编码花](https://zhuanlan.zhihu.com/c_1106228534267351040)<br/>
教程地址：<br/>
[1-做一个图片爬虫](https://zhuanlan.zhihu.com/p/68742061)<br/>
[2-并发症要治，还要有Future](https://zhuanlan.zhihu.com/p/68887845)
