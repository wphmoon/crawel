package com.skyblue.crawel.pojo;

import java.util.Date;

public class DownloadFile {
	String fileName;
	Date beginDate;
	Date endDate;
	
	
	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public Date getBeginDate() {
		return beginDate;
	}


	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}


	public Date getEndDate() {
		return endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	/**下载的时长（单位是毫秒）
	 * @return
	 */
	public long getDuration() {
		return (endDate.getTime()-beginDate.getTime());
	}
}
