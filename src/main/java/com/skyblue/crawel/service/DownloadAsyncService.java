package com.skyblue.crawel.service;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import com.skyblue.crawel.pojo.DownloadFile;
import com.skyblue.crawel.utils.DownloadUtils;


@Component
public class DownloadAsyncService {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Async
	public Future<DownloadFile> downloadImage(DownloadFile downloadFile,String filePath, String imgUrl, Map<String, String> requestPropMap) {
		 logger.info("====="+filePath);
		 DownloadUtils.downImages(filePath, imgUrl, requestPropMap);
		 downloadFile.setEndDate(new Date());
		 return new AsyncResult<DownloadFile> (downloadFile);
	}

	@Async
	public  Future<Date> PrintNumber() {
		for (int i = 1; i < 100; i++) {
			//logger.info(String.valueOf(i));
			System.out.print(i);
		}
		return new AsyncResult<Date> (new Date());
	}
}
