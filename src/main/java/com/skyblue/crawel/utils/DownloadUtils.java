package com.skyblue.crawel.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;




public class DownloadUtils {
	/**
     * 下载图片到指定目录
     *
     * @param filePath 文件路径
     * @param imgUrl   图片URL
     * @param requestPropMap request请求需要带的参数
     */
    public static void downImages(String filePath, String imgUrl,Map<String,String> requestPropMap) {
		// 若指定文件夹没有，则先创建
        File dir = new File(filePath);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        
        // 截取图片文件名
        String fileName = imgUrl.substring(imgUrl.lastIndexOf('/') + 1, imgUrl.length());

        try {
            // 文件名里面可能有中文或者空格，所以这里要进行处理。但空格又会被URLEncoder转义为加号
            String urlTail = URLEncoder.encode(fileName, "UTF-8");
            // 因此要将加号转化为UTF-8格式的%20
            imgUrl = imgUrl.substring(0, imgUrl.lastIndexOf('/') + 1) + urlTail.replaceAll("\\+", "\\%20");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        // 写出的路径
        File file = new File(filePath + File.separator + fileName);

        try {
            // 获取图片URL
            URL url = new URL(imgUrl);
            // 获得连接
            URLConnection connection = url.openConnection();
            if(requestPropMap != null) {
            //伪装成浏览器，否则会被403拒绝
	            for(String key:requestPropMap.keySet()){//keySet获取map集合key的集合  然后在遍历key即可
	            	connection.setRequestProperty(key, requestPropMap.get(key));
	             }
            }
            connection.setDoInput(true);
            // 设置10秒的相应时间
            connection.setConnectTimeout(10 * 1000);
            // 获得输入流
            InputStream in = connection.getInputStream();
            // 获得输出流
            BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file));
            // 构建缓冲区
            byte[] buf = new byte[1024];
            int size;
            // 写入到文件
            while (-1 != (size = in.read(buf))) {
                out.write(buf, 0, size);
            }
            out.close();
            in.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static String getHtml(String url) {
    	String subUrl = url.substring(url.lastIndexOf("/")+1);
    	subUrl = subUrl.substring(0, subUrl.lastIndexOf("."));
    	return subUrl;
    }
    
    public static void main(String[] args) throws URISyntaxException, IOException {
		String imgurl = "http://ac.meijiecao.net/ac/img/znb/meizitu/20190531_meizitu_20.jpg";
		downImages("d:/",imgurl,null);
		
    }
}
