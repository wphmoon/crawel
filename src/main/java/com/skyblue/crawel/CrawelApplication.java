package com.skyblue.crawel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.skyblue.crawel.service.DownloadAsyncService;
import com.skyblue.crawel.utils.DateUtils;

@SpringBootApplication
public class CrawelApplication {
	
	public static void testAsync(ConfigurableApplicationContext context) throws InterruptedException, ExecutionException {
		DownloadAsyncService service = context.getBean(DownloadAsyncService.class);
		List<Future<Date>> list = new ArrayList<Future<Date>>();
		for(int i=0;i<10;i++) {
			//service.downloadImage("","",null);
			//System.out.println(future.get());
			Future<Date> future = service.PrintNumber();
			list.add(future);
			TimeUnit.MICROSECONDS.sleep(10);
		}
		for(Future<Date> future:list) {
			while(future.isDone()) {
				System.out.println("------------"+DateUtils.dateTimeDetail(future.get()));
				break;
			}
		}
		System.out.println("完成----------------------------------------------");
	}

	public static void main(String[] args)  {
		ConfigurableApplicationContext context = SpringApplication.run(CrawelApplication.class, args);
//		try {
//			testAsync(context);
//		} catch (InterruptedException | ExecutionException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
}
